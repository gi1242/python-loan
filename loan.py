""" Simple interest loan amortization calculator

Example usage:

    from loan import *

    # Create a new loan and print the monthly payment and total interest
    cap1 = loan( 250000, 3.625, 15 )

    # Print all monthly balances, interest and cumulative interest
    cap1.print_monthly_interest()

    # Interest paid for the first year
    cap1.cumulative_interest[12]

    # Compute the cost of refinancing after 3 years
    (c, s) = refinance( cap1, 3*12, 4000, 2.75, years=15 )
"""
import math

class loan:
    """
        Create a new loan object. Once initialized, the following attributes
        are accessible.

        t                   Time (in months) to payoff.
        monthly_payment     Required monthly payment
        total_interest      Total interest paid over loan lifetime
        interest            Array containing monthly interest
        cumulative_interest Array containing cumulative monthly interest
    """

    def __init__( self, p, r, years=0, monthly_payment=0 ):
        """
            Create a new loan. Either the duration (in years) or the estimated
            monthly payment must be specified.
        """

        self.p = p # Principal
        self.r = r / 1200.0 # Monthly interest
        
        if years == 0 and monthly_payment > 0:
            self.t = -math.log( 1 - self.p * self.r/monthly_payment ) \
                / math.log(1 + self.r)
            self.monthly_payment = monthly_payment
        elif years > 0:
            self.t = int( round( years * 12) ) # Time in months
            self.monthly_payment = self.p * self.r / (1 - (1 + self.r)**-self.t)
        else:
            raise Exception('Must specify payments or duration')
    
        self.total_interest = self.monthly_payment * self.t - self.p
        print( 'Total interest: %.2f. Monthly payments: %.2f' 
                % (self.total_interest, self.monthly_payment) )
        
        self.monthly_balance = [p]
        self.interest = [0]
        self.cumulative_interest = [0]
        for n in range(1, int( math.ceil( self.t ) +1)):
            self.monthly_balance.append( max( 0,
                self.monthly_balance[n-1] * (1 + self.r)
                    - self.monthly_payment) )
            self.interest.append( self.monthly_balance[n-1] * self.r )
            self.cumulative_interest.append( self.cumulative_interest[n-1]
                    + self.interest[n])

    def print_monthly_interest(self):
        """ Print monthly balance, interest and cumulative interest """

        print( 'Month Balance    Interest  Cumulative interest' )
        for n in range( 1, len(self.monthly_balance)):
            print( '%5d %9.2f %9.2f %9.2f' % (n, self.monthly_balance[n],
                self.interest[n], self.cumulative_interest[n]))

def refinance( oldloan, month, fees, rate, years=0, monthly_payment=0,
        cash=0, tax_rate=.25, growth_rate=.03 ):
    """
        Compute the true cost of refinancing a loan.

        oldloan         : Your old loan
        month           : Month refinance started
        fees            : Fees charged by the new loan
        rate            : Rate of the new loan
        years           : Term of the new loan
        monthly_payment : Term of the new loan
        cash            : Cash you got when you refinanced
        tax_rate        : Your marginal tax bracket. Any interest paid on a
                          mortgage is tax deductible. We assume you reinvest it
                          to compute the final value.
        growth_rate     : Rate at which any savings from tax deductions (or
                          monthly payments) grow.

        Returns the new loan, unadjusted monthly savings and the adjusted
        cumulative savings.
    """
    
    newloan = loan( oldloan.monthly_balance[month] + fees + cash, rate,
            years=years, monthly_payment=monthly_payment )

    savings = [float(cash)]
    csavings = savings[0]
    dur = max(newloan.t, oldloan.t - month)
    for i in range( 1, dur+1 ):
        if i + month <= oldloan.t:
            omp = oldloan.monthly_payment
            oi = oldloan.interest[i+month]
        else:
            omp = 0
            oi = 0

        if i <= newloan.t:
            nmp = newloan.monthly_payment
            ni = newloan.interest[i]
        else:
            nmp = 0
            ni = 0

        s = (omp - nmp) - (oi - ni) * (tax_rate)
        savings.append(s)
        csavings = csavings * (1 + growth_rate/12) + s

    print( 'Annual tax adjusted savings' )
    nmonths = len( savings ) - 1
    for i in range( 0, (nmonths+11)//12 ):
        print( 'Year %3d: %.2f'
                % (i+1, sum( savings[(i*12+1):min( (i+1)*12, nmonths) ] ) ))
    print( 'Total inflation adjusted savings is %.2f in %d months'
            % (csavings, nmonths) )

    return (newloan, savings, csavings)
