# Simple python module to compute loan payments, and refinance costs

I wrote this since I had a hard time figuring out the true cost of refinancing
my mortgage, when you take into consideration taxes and inflation. Example
usage is as follows:

* Import the module:

        from loan import *

* Create a new loan and print the monthly payment and total interest

        cap1 = loan( 250000, 3.625, 15 )

* Print all monthly balances, interest and cumulative interest

        cap1.print_monthly_interest()

* Compute the interest paid in the first year

        cap1.cumulative_interest[12]

* Compute the cost of refinancing the loan after 3 years (3\*12 months) and
  paying 4000 in fees. This also assumes that you itemize deductions and get a
  tax refund on the interest you pay, and you invest all the money you save at
  the inflation rate. (Rates can be specified using the parameters if
  necessary.)

        (pnc, sav, csav) = refinance( cap1, 3*12, 4000, 2.75, years=15 )

* You can add optional arguments for extra cash paid to you when refinancing,
  your tax rate and the rate at which you expect your savings to grow.

        (pnc, sav, csav) = refinance( cap1, 3*12, 4000, 2.85, years=15
                cash=20000, tax_rate=.25, growth_rate=.03 )

* I recommend creating an ipython notebook so you can tinker with the numbers
  in a friendly web interface, instead of the python console...
